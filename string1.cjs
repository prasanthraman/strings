function string1(str) {
    if (typeof (str) != 'string') {
        return 0
    }
    let regEx = /[^-.\d]/; //any character that is not a dot ,minus or digit.
    if (str.replace('$', '').match(regEx)) {
        return 0
    }
    return parseFloat(str.replace('$', ''), 10)
}
module.exports = string1;