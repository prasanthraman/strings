function string3(str) {
    const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    let regEx = /^(0*[1-9]|[12][0-9]|3[01])[/]([1][012]|0*[1-9])[/]([0-9]{4})/

    if (typeof (str) != 'string') {
        return 0 //datatype no valid
    }
    if (!str.match(regEx)) {
        return 0 //date not valid
    }
    let monthIndex = str.split('/')[1]
    return months[parseInt(monthIndex) - 1]
}

module.exports = string3