
function string4(obj) {
    let fullName = []
    if (typeof (obj) != 'object' || Array.isArray(obj)) {
        return 0 //datatype no valid
    }
    for (const property in obj) {
        fullName.push(obj[property].charAt(0).toUpperCase() + obj[property].substr(1).toLowerCase())
    }
    return fullName.join(' ')
}
module.exports = string4;