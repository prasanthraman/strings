function string2(str) {
    if (typeof (str) != 'string') {
        return 0
    }
    let regEx = /[^.\d]/ //any character that is not a dot or digit.
    if (str.match(regEx)) {
        return [];
    }
    let components = str.split('.');
    for (let each in components) {
        if ((parseInt(components[each]) > 255) || parseInt(components[each] < 0) || components.length > 4) {
            //Invalid IP
            return [];
        }
        else {
            components[each] = parseInt(components[each])
        }
    }
    return components
}
module.exports = string2