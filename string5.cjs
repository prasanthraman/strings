function string5(obj) {
    if (typeof (obj) != 'object' || !Array.isArray(obj)) {
        return 0 //datatype no valid
    } if (obj.length == 0) {
        return []//Empty Array
    } else {
        return obj.join(' ')
    }

}
module.exports = string5;